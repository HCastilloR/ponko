// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PonkoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PONKO_API APonkoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
